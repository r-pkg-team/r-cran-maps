Source: r-cran-maps
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Chris Lawrence <lawrencc@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-maps
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-maps.git
Homepage: https://cran.r-project.org/package=maps
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev
Testsuite: autopkgtest-pkg-r

Package: r-cran-maps
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R support for producing geographic maps
 This package provides facilities for easily producing maps based on
 data sets in the GNU R statistical computing environment.
 .
 The r-cran-maps package includes map data for the United States
 (including state and county-level maps), New Zealand, and a world
 map; additional maps (including a higher-resolution world map) are
 available in the suggested r-cran-mapdata package.
 .
 The suggested r-cran-mapproj package adds facilities for calculating
 geographic projections, which are used by mapmakers to compensate for
 the inaccuracies inherent in projecting a spheroid's surface onto a
 two-dimensional plane.
